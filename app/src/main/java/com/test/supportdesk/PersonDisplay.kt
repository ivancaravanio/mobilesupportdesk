package com.test.supportdesk

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.NavUtils
import com.google.android.material.button.MaterialButton

class PersonDisplay : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.person_display_detailed)

        val toolbar = this.findViewById<Toolbar>(R.id.toolbar)
        val backNavigationTrigger = toolbar.findViewById<MaterialButton>(R.id.back_navigation_trigger)
        backNavigationTrigger.visibility = View.VISIBLE
        backNavigationTrigger.setOnClickListener {
            NavUtils.navigateUpFromSameTask(this)
        }

        this.setSupportActionBar(toolbar)

        this.supportActionBar?.apply {
            this.setDisplayShowTitleEnabled(false)
        }

        val person = this.intent.getParcelableExtra<Person>(PERSON_KEY)
        val personDisplayLite = PersonDisplayLite(this.findViewById(android.R.id.content), this)
        personDisplayLite.setPerson(person)
    }

    companion object {
        val PERSON_KEY = "person"
    }
}