package com.test.supportdesk

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class PeopleBrowserExtended : AppCompatActivity()
{
    private val _peopleModel = PeopleModel(this)

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.people_browser_extended)
        setSupportActionBar(this.findViewById(R.id.toolbar))

        _peopleModel.personProcessor = object : PeopleModel.AbstractPersonProcessor
        {
            override fun process(person: Person) {
                val intent = Intent(this@PeopleBrowserExtended, PersonDisplay::class.java).apply {
                    putExtra(PersonDisplay.PERSON_KEY, person)
                }

                this@PeopleBrowserExtended.startActivity(intent)
            }
        }

        _peopleModel.people = this.people

        val peopleBrowser = findViewById<RecyclerView>(R.id.people_browser)
        peopleBrowser.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        peopleBrowser.adapter = _peopleModel
    }

    private val people get() : List<Person>
    {
        val peopleAsJson = this.resources.openRawResource(R.raw.assets_api)
            .bufferedReader().use { it.readText() }

        // https://code.luasoftware.com/tutorials/android/gson-fromjson-list/
        val listType = object : TypeToken<List<Person>>() { }.type
        return Gson().fromJson(peopleAsJson, listType)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        this.menuInflater.inflate(R.menu.menu_main, menu)
        val searchView = menu.findItem(R.id.search).actionView as androidx.appcompat.widget.SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.setOnQueryTextListener(
            object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextChange(query: String): Boolean
                {
                    _peopleModel.filter.filter(query)
                    return true
                }

                override fun onQueryTextSubmit(query: String): Boolean = false
            } )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        return when (item.itemId)
        {
            R.id.search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}