package com.test.supportdesk

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person(
        @SerializedName("firstName")
        val firstName: String,
        @SerializedName("lastName")
        val lastName: String,
        @SerializedName("phone")
        val phoneNumber: String,
        @SerializedName("email")
        val emailAddress: String,
        @SerializedName("location")
        val location: String,
        @SerializedName("image")
        val avatarAddress: String,
        @SerializedName("available")
        val isAvailable: Boolean) : Parcelable
{
    val fullName get() = listOf(firstName, lastName).filter {s: String? -> !s.isNullOrEmpty()}.joinToString(" ")
}