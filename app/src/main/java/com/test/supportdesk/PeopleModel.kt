package com.test.supportdesk

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView

class PeopleModel(private var context: Context) : RecyclerView.Adapter<PeopleModel.ViewHolder>(), Filterable
{
    interface AbstractPersonProcessor
    {
        fun process(person: Person)
    }

    private var _people: MutableList<Person> = mutableListOf()

    var people : List<Person>
        get() = _people
        set(value)
        {
            _people = value.toMutableList()
            this.filterPeople()
        }

    private var filterCriteria: CharSequence? = null

    private fun filterPeople() {
        this.filter.filter(this.filterCriteria)
    }

    private var filteredPeople: List<Person> = listOf()
        set(value)
        {
            field = value
            this.notifyDataSetChanged()
        }

    var personProcessor : AbstractPersonProcessor? = null

    private fun Person.contains(filterCriteria: CharSequence) =
        listOf(
            this.firstName,
            this.lastName,
            this.location,
            this.emailAddress,
            this.phoneNumber,
            if (this.isAvailable) "available" else "busy").any {
            !it.isNullOrBlank() && it.contains(filterCriteria, true)
        }

    private fun filteredPeople(filterCriteria: CharSequence?) : List<Person> =
        if (filterCriteria.isNullOrBlank())
            this.people
        else this.people.filter {
           it.contains(filterCriteria)
        }

    override fun getItemCount(): Int = this.filteredPeople.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
    {
        val personConciseDisplay = LayoutInflater.from(parent.context).inflate(
            R.layout.person_display_concise,
            parent,
            false
        )
        return ViewHolder(personConciseDisplay)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
    {
        holder.personDisplay.setPerson(this.filteredPeople[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        val personDisplay: PersonDisplayLite = PersonDisplayLite(itemView, this@PeopleModel.context).apply { isDetailed = false }

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(p0: View?)
        {
            this@PeopleModel.personProcessor?.also {
                it.process(this@PeopleModel.filteredPeople[this.adapterPosition])
            }
        }
    }

    override fun getFilter() =
        object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults =
                FilterResults().apply { values = this@PeopleModel.filteredPeople(constraint) }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                this@PeopleModel.filterCriteria = constraint
                (results?.values as? List<Person>)?.also {
                    this@PeopleModel.filteredPeople = it
                }
            }
        }
}