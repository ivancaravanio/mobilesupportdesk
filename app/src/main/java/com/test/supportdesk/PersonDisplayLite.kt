package com.test.supportdesk

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class PersonDisplayLite(personDisplay: View, context: Context)
{
    private val _context = context
    private val _avatarDisplay: ImageView = personDisplay.findViewById(R.id.avatar_display)
    private val _availabilityIndicator: ImageView = personDisplay.findViewById(R.id.availability_indicator)
    private val _fullNameDisplay: TextView = personDisplay.findViewById(R.id.name_display)
    private val _locationDisplay: TextView = personDisplay.findViewById(R.id.location_display)
    private val _phoneNumberDisplay: TextView = personDisplay.findViewById(R.id.phone_number_display)
    private val _emailAddressDisplay: TextView = personDisplay.findViewById(R.id.email_address_display)

    fun setPerson(person: Person)
    {
        Glide.with(_context)
             .load(person.avatarAddress)
             .error(R.drawable.via_transparent)
             .into(_avatarDisplay)

        _availabilityIndicator.setImageResource( if (person.isAvailable) R.drawable.available else R.drawable.busy )

        _fullNameDisplay.text = person.fullName
        _locationDisplay.visibility = if (person.location.isNullOrBlank()) View.GONE else View.VISIBLE
        _locationDisplay.text = person.location
        _phoneNumberDisplay.text = String.format(this@PersonDisplayLite._context.resources.getString(R.string.phone_number_template), person.phoneNumber)
        _emailAddressDisplay.text = String.format(this@PersonDisplayLite._context.resources.getString(R.string.email_address_template), person.emailAddress)
    }

    var isDetailed : Boolean
        get() = _phoneNumberDisplay.visibility == View.VISIBLE
                && _emailAddressDisplay.visibility == View.VISIBLE

        set(value)
        {
            val visibility = if (value) View.VISIBLE else View.GONE
            listOf<View>( _phoneNumberDisplay, _emailAddressDisplay ).forEach {
                it.visibility = visibility
            }
        }
}